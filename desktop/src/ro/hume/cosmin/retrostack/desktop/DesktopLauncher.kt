package ro.hume.cosmin.retrostack.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import ro.hume.cosmin.retrostack.RetroStackGame

object DesktopLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.width = 450
        config.height = 800
        LwjglApplication(RetroStackGame(), config)
    }
}
