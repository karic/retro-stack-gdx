# Retro Stack

**Retro Stack** is a [libGDX](https://libgdx.badlogicgames.com/) game, inspired by [Luke Millar's](https://www.luketmillar.com/puzzles/stacks/).

<a href="https://f-droid.org/en/packages/ro.hume.cosmin.retrostack/">
  <img alt="Get it on F-Droid" src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="75" />
</a>

## How to play

Touch the screen (on mobile devices), press space or click the mouse (on desktop computers).

Place the moving bar over the last layer of the stack. Any bit of the bar that's not directly over the previous layer is lost.

## Credits

The UI is using the libGDX skin [Craftacular](https://github.com/czyzby/gdx-skins/tree/master/craftacular) by [Raymond "Raeleus" Buckley](https://ray3k.wordpress.com/software/skin-composer-for-libgdx/), licensed as [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/).
