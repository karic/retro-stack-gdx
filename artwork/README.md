# Logo Icons

# Android Launcher Icon
* `logo_108_72.svg`: on a 108x108 grid, use the 72x72 square in the center to allow cropping
* `logo_108_72.xml`: generated in Android Studio from the SVG

## GitLab Project Icon
* `logo_88_60.png`: on a 88x88 grid, use the 60x60 square in the center to allow cropping
