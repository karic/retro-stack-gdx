package ro.hume.cosmin.retrostack

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class CursorStackTest {

    private val cursor = Cursor(horizontalGridCells = 5, initialLength = 3)

    @Test
    @DisplayName("Add to empty stack")
    fun addToEmptyStack() {
        cursor.addToStack(mutableListOf())

        assertEquals(0, cursor.x)
        assertEquals(3, cursor.length)
    }

    /**
     * cursor: |x|x|x|o|o|
     * stack:  |=|=|=|o|o|
     */
    @Test
    @DisplayName("Fit to stack")
    fun fitToStack() {
        cursor.goToNextLevel()
        cursor.addToStack(mutableListOf(StackLevel(0, 0, 3)))

        assertEquals(0, cursor.x)
        assertEquals(3, cursor.length)
    }

    /**
     * cursor: |x|x|x|o|o|
     * stack:  |o|=|=|o|o|
     */
    @Test
    @DisplayName("Miss one to the left")
    fun missOneToTheLeft() {
        cursor.goToNextLevel()
        cursor.addToStack(mutableListOf(StackLevel(1, 0, 2)))

        assertEquals(1, cursor.x)
        assertEquals(2, cursor.length)
    }

    /**
     * cursor: |x|x|x|o|o|
     * stack:  |=|=|o|o|o|
     */
    @Test
    @DisplayName("Miss one to the right")
    fun missOneToTheRight() {
        cursor.goToNextLevel()
        cursor.addToStack(mutableListOf(StackLevel(0, 0, 2)))

        assertEquals(0, cursor.x)
        assertEquals(2, cursor.length)
    }

    /**
     * cursor: |x|x|x|o|o|
     * stack:  |o|o|o|=|=|
     */
    @Test
    @DisplayName("Miss the stack")
    fun missAll() {
        cursor.goToNextLevel()
        assertThrows(CursorNotOverStackException::class.java) {
            cursor.addToStack(mutableListOf(StackLevel(3, 0, 2)))
        }
        assertEquals(0, cursor.x)
        assertEquals(3, cursor.length)
    }
}
