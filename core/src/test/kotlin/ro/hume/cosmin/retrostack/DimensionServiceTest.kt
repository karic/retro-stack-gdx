package ro.hume.cosmin.retrostack

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName

import org.junit.jupiter.api.Test

class DimensionServiceTest {

    private val dimensionService = DimensionService()

    @Test
    @DisplayName("VGA (640x480)")
    fun vga() {
        val dimension = dimensionService.computeDimensions(screenWidth = 480, screenHeight = 640)
        assertEquals(Dimension(elementSize = 17, spaceSize = 4, margin = 3), dimension)
    }
}
