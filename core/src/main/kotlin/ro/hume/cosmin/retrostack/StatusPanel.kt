package ro.hume.cosmin.retrostack

import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align

class StatusPanel(private val game: RetroStackGame) : Group() {

    private var rowHeight = 0f
    private var colWidth = 0f

    private lateinit var label: Label

    override fun setStage(stage: Stage?) {
        super.setStage(stage)

        if (stage != null) {
            rowHeight = stage.width / 12
            colWidth = stage.width / 12

            addScoreLabel()
        }
    }

    private fun addScoreLabel() {
        label = Label(game.score.toString(), game.skin, "xp")
        label.setSize(stage.width, rowHeight)
        label.setPosition(0f, stage.height - rowHeight)
        label.setAlignment(Align.right)

        addActor(label)
    }

    override fun act(delta: Float) {
        super.act(delta)
        label.setText(game.score.toString())
    }
}
