package ro.hume.cosmin.retrostack

data class Dimension(val elementSize: Int, val spaceSize: Int, val margin: Int)
