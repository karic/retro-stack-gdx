package ro.hume.cosmin.retrostack

import kotlin.math.floor
import kotlin.math.roundToInt

class DimensionService {

    companion object {
        const val HORIZONTAL_ELEMENT_COUNT = 15
        const val VERTICAL_ELEMENT_COUNT = 30
        /** The size of a space, as the percentage out of an element's size. */
        const val SPACE_SIZE_PERCENT = 0.25
    }

    fun computeDimensions(screenWidth: Int, screenHeight: Int): Dimension {
        val horizontalDimension = computeDimension(screenWidth, HORIZONTAL_ELEMENT_COUNT)
        val verticalDimension = computeDimension(screenHeight, VERTICAL_ELEMENT_COUNT)
        return if (horizontalDimension.elementSize < verticalDimension.elementSize)
            horizontalDimension
        else
            verticalDimension
    }

    /**
     * available space = element_count * element_size + space_count * space_size
     *
     * available space = element_count * element_size
     *                   + (element_count + 1) * element_size * space_between_elements
     *
     *                available_space
     * element_size = --------------------------------------------------------------
     *                (element_count + (element_count + 1) * space_between_elements)
     */
    private fun computeDimension(availableSpace: Int, elementCount: Int): Dimension {
        val elementSize = availableSpace / (elementCount + (elementCount + 1) * SPACE_SIZE_PERCENT)
        val spaceSize = elementSize * SPACE_SIZE_PERCENT

        var intElementSize = elementSize.roundToInt()
        var intSpaceSize = spaceSize.roundToInt()

        var totalSize = intElementSize * elementCount + intSpaceSize * (elementCount + 1)
        if (totalSize > availableSpace) {
            intElementSize = floor(elementSize).toInt()
            intSpaceSize = floor(spaceSize).toInt()

            totalSize = intElementSize * elementCount + intSpaceSize * (elementCount + 1)
        }

        val margin = (availableSpace - totalSize) / 2

        return Dimension(elementSize = intElementSize, spaceSize = intSpaceSize, margin = margin)
    }
}
