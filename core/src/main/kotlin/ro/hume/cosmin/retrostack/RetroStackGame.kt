package ro.hume.cosmin.retrostack

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.ui.Skin

class RetroStackGame : Game() {

    companion object {
        const val WORLD_WIDTH = 450f
        const val WORLD_HEIGHT = 800f
    }

    private lateinit var font: BitmapFont

    private lateinit var batch: SpriteBatch

    lateinit var shapeRenderer: ShapeRenderer
        private set

    lateinit var skin: Skin
        private set

    var score = 0
        private set

    override fun create() {
        font = BitmapFont()
        batch = SpriteBatch()
        shapeRenderer = ShapeRenderer()
        skin = Skin(Gdx.files.internal("skin/craftacular-ui.json"))

        setScreen(TitleScreen(this))
    }

    override fun dispose() {
        skin.dispose()
        shapeRenderer.dispose()
        font.dispose()
        batch.dispose()
    }

    fun addScore(value: Int) {
        score += value
    }

    fun resetScore() {
        score = 0
    }
}
