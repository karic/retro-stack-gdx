package ro.hume.cosmin.retrostack

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.FitViewport

class TitleScreen(private val game: RetroStackGame) : ScreenAdapter() {

    private val stage: Stage = Stage(FitViewport(RetroStackGame.WORLD_WIDTH, RetroStackGame.WORLD_HEIGHT))
    private var rowHeight = 0f
    private var colWidth = 0f

    init {
        rowHeight = stage.width / 12
        colWidth = stage.width / 12

        addTitleLabel()
        addPlayButton()
        addQuitButton()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    private fun addTitleLabel() {
        val label = Label("Retro\nStack", game.skin, "title")
        label.setSize(stage.width, rowHeight)
        label.setPosition(0f, stage.height - rowHeight * 4)
        label.setAlignment(Align.center)
        stage.addActor(label)
    }

    private fun addPlayButton() {
        val button = TextButton("Play", game.skin, "default")
        button.setSize(colWidth * 4, rowHeight)
        button.setPosition(colWidth * 4, stage.height - rowHeight * 10)
        button.addListener(object : InputListener() {

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                game.screen = GameScreen(game)
            }

            override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                return true
            }
        })
        stage.addActor(button)
    }

    private fun addQuitButton() {
        val button = TextButton("Quit", game.skin, "default")
        button.setSize(colWidth * 4, rowHeight)
        button.setPosition(colWidth * 4, stage.height - rowHeight * 12)
        button.addListener(object : InputListener() {

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                Gdx.app.exit()
            }

            override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                return true
            }
        })
        stage.addActor(button)
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, .25f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.act()
        stage.draw()
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun dispose() {
        stage.dispose()
    }
}
